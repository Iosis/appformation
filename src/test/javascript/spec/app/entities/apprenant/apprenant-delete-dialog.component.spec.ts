/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppFormationTestModule } from '../../../test.module';
import { ApprenantDeleteDialogComponent } from 'app/entities/apprenant/apprenant-delete-dialog.component';
import { ApprenantService } from 'app/entities/apprenant/apprenant.service';

describe('Component Tests', () => {
  describe('Apprenant Management Delete Component', () => {
    let comp: ApprenantDeleteDialogComponent;
    let fixture: ComponentFixture<ApprenantDeleteDialogComponent>;
    let service: ApprenantService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppFormationTestModule],
        declarations: [ApprenantDeleteDialogComponent]
      })
        .overrideTemplate(ApprenantDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ApprenantDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ApprenantService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
