/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nsis.web.rest.vm;
