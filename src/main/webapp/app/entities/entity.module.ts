import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'cours',
        loadChildren: './cours/cours.module#AppFormationCoursModule'
      },
      {
        path: 'apprenant',
        loadChildren: './apprenant/apprenant.module#AppFormationApprenantModule'
      },
      {
        path: 'promo',
        loadChildren: './promo/promo.module#AppFormationPromoModule'
      },
      {
        path: 'formation',
        loadChildren: './formation/formation.module#AppFormationFormationModule'
      },
      {
        path: 'niveau',
        loadChildren: './niveau/niveau.module#AppFormationNiveauModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppFormationEntityModule {}
