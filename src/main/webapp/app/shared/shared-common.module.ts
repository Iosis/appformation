import { NgModule } from '@angular/core';

import { AppFormationSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [AppFormationSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [AppFormationSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class AppFormationSharedCommonModule {}
