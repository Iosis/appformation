import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppFormationSharedLibsModule, AppFormationSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [AppFormationSharedLibsModule, AppFormationSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [AppFormationSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppFormationSharedModule {
  static forRoot() {
    return {
      ngModule: AppFormationSharedModule
    };
  }
}
